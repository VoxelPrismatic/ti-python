import math
import cmath
import time
import random
from keys import *

##/// BASIC I/O

def Disp(*what): print(x for x in what)
def Input(text="?"): return input(text)

##/// LISTS, MATRICIES, MORE

def ΔList(foo): return list.dList(foo)
def augment(l1,l2,v=False): return list.augment(l1,l2,v)

##/// MATHS

def gcd(v1, v2): return gcf_lcm.gcd(v1, v2)
def lcm(v1, v2): return gcf_lcm.lcm(v1, v2)

def _1(n:float): return n**-1
def _2(n:float): return n**2
def _3(n:float): return n**3
def _(n:float,b:float): return n**b

def fPart(n:float): return n % 1
def iPart(n:float): return n - n%1

def _2R(n:float): return n**.5
def _3R(n:float): return n**(3**-1)
def _xR(b:float,n:float): return n**(b**-1)

def π(): return math.pi

def angle(n): return cmath.polar(n)[1]
def conj(x): return x.conjugate()
def remainder(x,y): return math.remainder(x,y)
def ceiling(x): return basic.ceiling(x)
def floor(x): return basic.floor(x)

def RPθ(n,l): return basic.RP0(n,l)
def PRx(x,y): return basic.PRx(x,y)
def PRy(x,y): return basic.PRy(x,y)
def RPr(x,y): return ((x**2)+(y**2))**.5

##/// IMAGINARY

def imag(num): return math.imag(num)
def real(num): return math.real(num)
def i(hel:str): return eval(hel.replace("i","j"))

##/// LOGIC

def iE(n,s): return n==s # x=y
def nE(n,s): return n!=s # x≠y
def gT(n,s): return n >s # x>y
def lT(n,s): return n <s # x<y
def gE(n,s): return n>=s # x≥y
def lE(n,s): return n<=s # x≤y

def xor(n,s): return (n or s) and not (n and s)

##/// BASIC
def For(lw, hi, *arg): basic.For(lw,hi,arg) # The python "for" is NOT the same
def startTmr(): return time.monotonic()
def checkTmr(a): return time.monotonic()-a
def length(a): return len(a)
def eval(a): return str(a)
def expr(a): return eval(a)
def inString(main, sub): return main.find(sub)+1
def sub(*args): return basic.sub(args)
def randInt(x, y): return random.randint(x,y)
def toString(a): return str(a)
def randMat(x:int,y:int): return rand.randMat(x,y)
def char(x): return basic.char(x)
def _ord(x): return basic._ord(x)