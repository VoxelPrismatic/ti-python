import math

def ceiling(x):
    if type(x) == list: return [math.ceiling(y) for y in x]
    else: return math.ceiling(x)

def floor(x):
    if type(x) == list: return [math.floor(y) for y in x]
    else: return math.floor(x)

def char(x):
    if type(x) == int: return chr(x)
    elif type(x) == list: return [chr(y) for y in x]

def _ord(x):
    if type(x) == str: return ord(x[0])
    elif type(x) == list: return [ord y for y in x]

def For(lw, hi, arg):
    y = lw; hi+=1
    if len(arg)==1: code=arg[0]; stp=1
    else: stp, code=arg
    for x in range(lw,hi,stp):
        exec(code); y+=1
        if y>=hi: break

def sub(arg):
    if len(arg)==3: return arg[0][arg[1]-1:arg[1]-arg[2]-1]
    else: return float(arg[0])/100

def RP0(n,l):
    if type(l) != list: return math.tan(l/n)
    else: return [math.tan(l/x) for x in n]

def PRx(x,y):
    if type(x) == type(y) != list: return x*math.cos(y)
    else: return [x[z]*math.cos(y[z]) for z in len(x)]

def PRy(x,y):
    if type(x) == type(y) != list: return x*math.sin(y)
    else: return [x[z]*math.sin(y[z]) for z in len(x)]