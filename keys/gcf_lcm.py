import math

def gcd(var1, var2):
    try: return math.gcd(var1, var2)
    except TypeError: pass
    try:
        if len(var1)==len(var2):
            ret = []
            for itm in range(len(var1)):
                ret.append(math.gcd(var1[itm],var2[itm]))
            return ret
        else: raise "DIM MISMATCH"
    except "DIM MISMATCH" as err: return err

def lcm(var1, var2):
    try: return math.lcm(var1, var2)
    except TypeError: pass
    try:
        if len(var1)==len(var2):
            ret = []
            for itm in range(len(var1)):
                ret.append(math.lcm(var1[itm],var2[itm]))
            return ret
        else: raise "DIM MISMATCH"
    except "DIM MISMATCH" as err: return err