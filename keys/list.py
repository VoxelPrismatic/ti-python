def dList(foo):
    if len(foo)==1: return Exception("INVALID DIM")
    try: 
        chng = []
        for x in range(1, len(foo)):
            chng.append(foo[x]-foo[x-1])
    except: return Exception("SYNTAX ERROR")

def augment(l1,l2,v=True):
    if not v: 
        l1.append(l2)
        return l1
    elif len(l1)+len(l2) > 99: return Exception("INVALID DIM")
    try: 
        l1[0][0]
        try: l2[0][0]
        except: return Exception("DATA TYPE")
    except:
        if len(l2)==1: return Exception("DATA TYPE")
        return l1.append(l2)
    if len(l1) != len(l2): return Exception("DIM MISMATCH")
    l3=[]
    for x in range(len(l2)):
      for y in range(len(l2[x])):
        l3.append([l1[x].pop(0),l2[x].pop(0)])
    return l3